package com.thoughtworks.paysa.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Transaction {
    private Date date;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private Date creationDate;

    private String remarks;

    @Enumerated(EnumType.STRING)
    private TransactionType type;

    private double amount;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "wallet_id", nullable = false)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Wallet wallet;

    private String fromToWallet;
    public Transaction() {
    }

    public Transaction(Date date, String remarks, TransactionType type, double amount) {
        this.creationDate = date;
        this.remarks = remarks;
        this.type = type;
        this.amount = amount;
        this.fromToWallet = "";
    }

    public Transaction(Date date, String remarks, TransactionType type, double amount, Wallet wallet) {
        this.creationDate = date;
        this.remarks = remarks;
        this.type = type;
        this.amount = amount;
        this.wallet = wallet;
        this.fromToWallet = "";
    }

    public String getFromToWallet() {
        return fromToWallet;
    }

    public void setFromToWallet(String fromToWallet) {
        this.fromToWallet = fromToWallet;
    }

    public Transaction(Date date, String remarks, TransactionType type, double amount, Wallet wallet, String fromToWallet) {
        this.creationDate = date;
        this.remarks = remarks;
        this.type = type;
        this.amount = amount;
        this.wallet = wallet;
        this.fromToWallet = fromToWallet;
    }

    public static Transaction selfTransaction(Wallet wallet, Double balance) {
        final Transaction transaction = new Transaction(new Date(),"Self", TransactionType.CREDIT, balance);
        transaction.setWallet(wallet);
        return transaction;
    }

    public void setWallet(Wallet wallet) {
        this.wallet = wallet;
    }

    public int getId() {
        return id;
    }

    public Wallet getWallet() {
        return wallet;
    }

    public TransactionType getType() {
        return type;
    }

    public double getAmount() {
        return amount;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getRemarks() {
        return remarks;
    }
}
