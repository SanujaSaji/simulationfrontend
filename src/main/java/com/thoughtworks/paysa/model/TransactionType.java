package com.thoughtworks.paysa.model;

public enum TransactionType {
    CREDIT, DEBIT;
}
