package com.thoughtworks.paysa.controller;

import com.thoughtworks.paysa.exception.WalletNotFoundException;
import com.thoughtworks.paysa.model.Wallet;
import com.thoughtworks.paysa.service.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("users/{userId}/wallets")
public class WalletController {

    @Autowired
    private WalletService walletService;

    @GetMapping(value = "/{walletId}")
    @ResponseStatus(HttpStatus.OK)
    Wallet get(@PathVariable int walletId) throws WalletNotFoundException {
        return walletService.get(walletId);
    }

    @PutMapping(value = "/{walletId}")
    @ResponseStatus(HttpStatus.OK)
    Wallet addMoney(@PathVariable int walletId, @RequestBody Wallet wallet) throws WalletNotFoundException {
        return walletService.addMoney(walletId, wallet.getBalance());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    Wallet create(@Valid @RequestBody Wallet wallet) {
        return walletService.save(wallet);
    }

    @GetMapping(value="")
    @ResponseStatus(HttpStatus.OK)
    List<Wallet> list(@PathVariable int userId) {
        return walletService.getByUserId(userId);
    }
}