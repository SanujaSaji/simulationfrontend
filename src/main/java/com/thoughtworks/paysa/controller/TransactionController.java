package com.thoughtworks.paysa.controller;

import com.thoughtworks.paysa.exception.InvalidTransaction;
import com.thoughtworks.paysa.exception.WalletNotFoundException;
import com.thoughtworks.paysa.exception.ZeroTransactionException;
import com.thoughtworks.paysa.model.Transaction;
import com.thoughtworks.paysa.model.TransferMoney;
import com.thoughtworks.paysa.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/users/{userId}/wallets/{walletId}")
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    @PostMapping("/transactions")
    @ResponseStatus(HttpStatus.CREATED)
    Transaction create(@Valid @PathVariable int walletId, @RequestBody Transaction transaction) throws WalletNotFoundException, InvalidTransaction {
        return transactionService.save(walletId, transaction);
    }

    @GetMapping("/transactions")
    @ResponseStatus(HttpStatus.OK)
    List<Transaction> list(@PathVariable int walletId) {
        return transactionService.getByWalletId(walletId);
    }

    @PostMapping("/transfer")
    @ResponseStatus(HttpStatus.OK)
    Transaction transferMoney(@RequestBody TransferMoney transferMoney) throws InvalidTransaction, WalletNotFoundException {
        return transactionService.transfer(transferMoney);
    }
}