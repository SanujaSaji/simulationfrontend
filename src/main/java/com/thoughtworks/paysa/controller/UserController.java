package com.thoughtworks.paysa.controller;

import com.thoughtworks.paysa.model.User;
import com.thoughtworks.paysa.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    List<User> list() {
        return userService.getAll();
    }
}
