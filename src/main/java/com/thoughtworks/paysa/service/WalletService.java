package com.thoughtworks.paysa.service;

import com.thoughtworks.paysa.exception.InvalidTransaction;
import com.thoughtworks.paysa.exception.WalletByNameAlreadyExists;
import com.thoughtworks.paysa.exception.WalletNotFoundException;
import com.thoughtworks.paysa.model.Transaction;
import com.thoughtworks.paysa.model.Wallet;
import com.thoughtworks.paysa.repository.WalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class WalletService {
    @Autowired
    private WalletRepository walletRepository;

    @Autowired
    private TransactionService transactionService;

    public Wallet get(int walletId) throws WalletNotFoundException {
        return walletRepository.findById(walletId).orElseThrow(WalletNotFoundException::new);
    }

    public Wallet addMoney(int id, double amount) throws WalletNotFoundException {
        Wallet wallet = walletRepository.findById(id).orElseThrow(WalletNotFoundException::new);
        wallet.credit(amount);
        return walletRepository.save(wallet);
    }

    public Wallet save(Wallet wallet) throws WalletByNameAlreadyExists {
        Wallet savedWallet;
        wallet.setCreationDate(new Date());
        try {
            savedWallet = walletRepository.save(wallet);
        } catch (DataIntegrityViolationException e) {
            throw new WalletByNameAlreadyExists();
        }
        try {
            Transaction transaction = Transaction.selfTransaction(savedWallet, savedWallet.getBalance());
            transactionService.save(savedWallet.getId(), transaction);
        } catch (WalletNotFoundException | InvalidTransaction ignored) {
        }
        return savedWallet;
    }

    public List<Wallet> getByUserId(int userId) {
        List<Wallet> wallets = walletRepository.findByUserId(userId);
        wallets.sort((o1, o2) -> o2.getCreationDate().compareTo(o1.getCreationDate()));
        return wallets;
    }
}