package com.thoughtworks.paysa.service;

import com.thoughtworks.paysa.model.User;
import com.thoughtworks.paysa.model.Wallet;
import com.thoughtworks.paysa.repository.UserRepository;
import com.thoughtworks.paysa.repository.WalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
public class SeedService {
    @Autowired
    public UserRepository userRepository;

    @Autowired
    public WalletRepository walletRepository;

    @EventListener
    public void seed(ContextRefreshedEvent event) {
        seedUser();
        seedWallet();
    }

    public User seedUser() {
        return userRepository.save(new User("admin", "password", "John", "Doe"));
    }

    public Wallet seedWallet() {
        User user = userRepository.save(new User("seed-wallet-user", "password", "John", "Doe"));
        Wallet adminWallet = new Wallet("admin-wallet", 100, user);
        return walletRepository.save(adminWallet);
    }
}
