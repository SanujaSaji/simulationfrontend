package com.thoughtworks.paysa.service;

import com.thoughtworks.paysa.exception.WalletByNameAlreadyExists;
import com.thoughtworks.paysa.model.Transaction;
import com.thoughtworks.paysa.model.User;
import com.thoughtworks.paysa.model.Wallet;
import com.thoughtworks.paysa.repository.TransactionRepository;
import com.thoughtworks.paysa.repository.UserRepository;
import com.thoughtworks.paysa.repository.WalletRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
@AutoConfigureMockMvc
class WalletServiceTest {

    @Autowired
    private
    WalletService walletService;

    @MockBean
    private
    TransactionService transactionService;

    @Autowired
    private
    WalletRepository walletRepository;

    @Autowired
    private
    TransactionRepository transactionRepository;

    @Autowired
    private
    UserRepository userRepository;

    @BeforeEach
    void clearRepositories() {
        transactionRepository.deleteAll();
        walletRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    void shouldGetMoneyFromWallet() throws Exception {
        User user = new User("admin", "password", "John", "Doe");
        user = userRepository.save(user);
        Wallet adminWallet = new Wallet("admin-wallet", 100, user);
        walletRepository.save(adminWallet);

        Wallet wallet = walletService.get(adminWallet.getId());

        assertEquals(adminWallet.getId(), wallet.getId());
        assertEquals(adminWallet.getBalance(), wallet.getBalance());
        assertEquals(adminWallet.getName(), wallet.getName());
    }

    @Test
    void shouldAddMoneyToWalletAndUpdateBalance() throws Exception {
        User user = new User("admin", "password", "John", "Doe");
        user = userRepository.save(user);
        System.out.println(userRepository.count());
        Wallet adminWallet = new Wallet("admin-wallet", 100, user);
        System.out.println(walletRepository.count());
        walletRepository.save(adminWallet);

        walletService.addMoney(adminWallet.getId(), 200);

        assertEquals(300, walletRepository.findById(adminWallet.getId()).get().getBalance());
    }

    @Test
    void shouldAddWalletIfNameNotAlreadyExists() {
        User user = new User("admin", "password", "John", "Doe");
        user = userRepository.save(user);
        Wallet wallet = new Wallet("wallet", 100, user);

        walletService.save(wallet);

        Wallet savedWallet = walletRepository.findById(wallet.getId()).orElse(null);
        assertEquals(wallet.getBalance(), savedWallet.getBalance());
        assertEquals(wallet.getName(), savedWallet.getName());
    }

    @Test
    void shouldNotAddWalletIfNameAlreadyExists() {
        User user = new User("admin", "password", "John", "Doe");
        user = userRepository.save(user);
        Wallet wallet = new Wallet("Payment wallet", 100, user);
        walletService.save(wallet);

        Wallet walletWithSameName = new Wallet("Payment wallet", 10, user);
        assertThrows(WalletByNameAlreadyExists.class, () -> walletService.save(walletWithSameName));
    }

    @Test
    void shouldCreateTransactionWhenAddingWallet() throws Exception {
        User user = new User("admin", "password", "John", "Doe");
        user = userRepository.save(user);
        Wallet wallet = new Wallet("Payment wallet", 100, user);
        when(transactionService.save(wallet.getId(), new Transaction())).thenReturn(new Transaction());

        walletService.save(wallet);

        verify(transactionService, times(1)).save(any(Integer.class), any(Transaction.class));
    }
}
