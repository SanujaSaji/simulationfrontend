package com.thoughtworks.paysa.service;


import com.thoughtworks.paysa.model.User;
import com.thoughtworks.paysa.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@AutoConfigureMockMvc
class UserServiceTest {
    @Autowired
    UserRepository userRepository;

    @Test
    void shouldGetAllUsers() throws Exception {
        User user = userRepository.save(new User("admin", "password", "John", "Doe"));

        final List<User> users = userRepository.findAll();

        assertTrue(users.size() > 0);
        assertEquals(user.getId(), users.get(users.size() - 1).getId());
    }
}
