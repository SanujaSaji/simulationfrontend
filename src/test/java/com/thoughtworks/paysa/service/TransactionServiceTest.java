package com.thoughtworks.paysa.service;

import com.thoughtworks.paysa.exception.InvalidTransaction;
import com.thoughtworks.paysa.exception.ZeroTransactionException;
import com.thoughtworks.paysa.model.Transaction;
import com.thoughtworks.paysa.model.TransferMoney;
import com.thoughtworks.paysa.model.User;
import com.thoughtworks.paysa.model.Wallet;
import com.thoughtworks.paysa.repository.TransactionRepository;
import com.thoughtworks.paysa.repository.UserRepository;
import com.thoughtworks.paysa.repository.WalletRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.List;

import static com.thoughtworks.paysa.model.TransactionType.CREDIT;
import static com.thoughtworks.paysa.model.TransactionType.DEBIT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@AutoConfigureMockMvc
class TransactionServiceTest {

    @Autowired
    private
    TransactionService transactionService;

    @Autowired
    private
    TransactionRepository transactionRepository;

    @Autowired
    private
    WalletRepository walletRepository;

    @Autowired
    private
    UserRepository userRepository;

    @Test
    void shouldAddMoneyToWalletAndUpdateBalance() throws Exception {
        User user = userRepository.save(new User("admin", "password", "John", "Doe"));
        Wallet adminWallet = new Wallet("admin-wallet", 100, user);
        walletRepository.save(adminWallet);
        Transaction transaction = new Transaction(new Date(), "Self", CREDIT, 300.0);

        transactionService.save(adminWallet.getId(), transaction);

        assertEquals(CREDIT, transactionRepository.findById(transaction.getId()).get().getType());
        assertEquals(300, transactionRepository.findById(transaction.getId()).get().getAmount());
        assertEquals("Self", transactionRepository.findById(transaction.getId()).get().getRemarks());
    }

    @Test
    void shouldGetTransactionsByWalletId() {
        User user = userRepository.save(new User("admin", "password", "John", "Doe"));
        Wallet adminWallet = new Wallet("admin-wallet", 100, user);
        Transaction transaction = new Transaction(new Date(), "Self", CREDIT, 100.0);
        walletRepository.save(adminWallet);
        transaction.setWallet(adminWallet);
        Transaction savedTransaction = transactionRepository.save(transaction);

        final List<Transaction> transactions = transactionRepository.findByWalletId(adminWallet.getId());

        assertEquals(1, transactions.size());
        assertEquals(savedTransaction.getId(), transactions.get(0).getId());
    }

    @Test
    void shouldNotCreateTransactionForZeroAmount() {
        User user = userRepository.save(new User("admin", "password", "John", "Doe"));
        Wallet adminWallet = new Wallet("admin-wallet", 0, user);
        Transaction transaction = new Transaction(new Date(), "Self", CREDIT, 0);
        walletRepository.save(adminWallet);

        assertThrows(InvalidTransaction.class, () -> transactionService.save(adminWallet.getId(),transaction));
    }

    @Test
    void shouldCreateTwoTransactionsForTransferMoneyBetweenWallets() throws Exception {
        User user = userRepository.save(new User("admin", "password", "John", "Doe"));
        Wallet fromWallet = new Wallet("fromWallet", 100, user);
        Wallet toWallet = new Wallet("toWallet", 0, user);
        walletRepository.save(fromWallet);
        walletRepository.save(toWallet);

        Transaction transaction = new Transaction(new Date(), "Transfer", DEBIT, 100);
        TransferMoney transferMoney = new TransferMoney(fromWallet, toWallet, transaction);

        Transaction savedTransaction = transactionService.transfer(transferMoney);

        Transaction debitTransaction = new Transaction(new Date(), "Transfer", DEBIT, 100);
        Transaction creditTransaction = new Transaction(new Date(), "Transfer", CREDIT, 100);
        creditTransaction.setWallet(toWallet);
        debitTransaction.setWallet(fromWallet);

        assertEquals(savedTransaction.getAmount(), debitTransaction.getAmount());
        assertEquals(savedTransaction.getType(), debitTransaction.getType());
        assertEquals(savedTransaction.getRemarks(), debitTransaction.getRemarks());
    }
}
