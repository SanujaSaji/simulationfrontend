package com.thoughtworks.paysa.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.paysa.model.*;
import com.thoughtworks.paysa.repository.UserRepository;
import com.thoughtworks.paysa.repository.WalletRepository;
import com.thoughtworks.paysa.service.TransactionService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.Arrays;
import java.util.Date;

import static com.thoughtworks.paysa.model.TransactionType.CREDIT;
import static com.thoughtworks.paysa.model.TransactionType.DEBIT;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class TransactionControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private
    TransactionService transactionService;

    @Autowired
    private
    UserRepository userRepository;

    @Autowired
    private
    WalletRepository walletRepository;

    @Test
    void shouldPostTransaction() throws Exception {
        User user = userRepository.save(new User("admin", "password", "John", "Doe"));
        Wallet wallet = new Wallet("wallet", 400, user);
        ObjectMapper objectMapper = new ObjectMapper();
        walletRepository.save(wallet);

        Transaction transaction = new Transaction(new Date(), "Self", CREDIT, 300.0);
        when(transactionService.save(wallet.getId(), transaction)).thenReturn(transaction);

        mockMvc.perform(post("/users/1/wallets/2/transactions")
                .content(objectMapper.writeValueAsString(transaction))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        verify(transactionService, times(1)).save(any(Integer.class), any(Transaction.class));
    }

    @Test
    void shouldGetTransactionsForWalletWithId2() throws Exception {
        Transaction transaction = new Transaction(new Date(), "Self", CREDIT, 100.0);
        when(transactionService.getByWalletId(eq(2))).thenReturn(Arrays.asList(transaction));

        final ResultActions getTransactionsByWalletId = mockMvc.perform(get("/users/1/wallets/2/transactions")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        verify(transactionService, times(1)).getByWalletId(eq(2));

        getTransactionsByWalletId
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].remarks").value("Self"))
                .andExpect(jsonPath("$[0].type").value("CREDIT"))
                .andExpect(jsonPath("$[0].amount").value("100.0"));
    }

    @Test
    void shouldTransferMoneyBetweenWalletsOfSameUser() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        Wallet fromWallet = mock(Wallet.class);
        when(fromWallet.getId()).thenReturn(2);
        Wallet toWallet = mock(Wallet.class);
        Transaction transaction = new Transaction(new Date(), "Remarks", DEBIT, 100);
        TransferMoney transferMoney = new TransferMoney(fromWallet, toWallet, transaction);

        when(transactionService.transfer(transferMoney)).thenReturn(transaction);

        mockMvc.perform(post("/users/1/wallets/" + fromWallet.getId() + "/transfer")
                .content(objectMapper.writeValueAsString(transaction))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(transactionService, times(1)).transfer(any(TransferMoney.class));
    }
}
